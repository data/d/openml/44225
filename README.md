# OpenML dataset: MembershipWoes

https://www.openml.org/d/44225

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A certain premium club boasts a large customer membership. The members pay an annual membership fee in return for using the exclusive facilities offered by this club. The fees are customized for every member's personal package. In the last few years, however, the club has been facing an issue with a lot of members cancelling their memberships. The club management plans to address this issue by proactively addressing customer grievances. They, however, do not have enough bandwidth to reach out to the entire customer base individually and are looking to see whether a statistical approach can help them identify customers at risk. The aim is to help them identify these customers.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44225) of an [OpenML dataset](https://www.openml.org/d/44225). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44225/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44225/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44225/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

